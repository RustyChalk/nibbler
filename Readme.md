# Nibbler

A small library to handle nibbles.

There are a few other libraries on Crates.io which handle nibbles.

## Efficiency

Nothing in this library should be considered "efficient," nor "fast". It is however meant to be simple.

## Dependencies

* None, Nil

Only thing needed is the `std::fmt::Display` _(standard library)_ for displaying objects.

## Quick Example Usage

```rust
use nibbler::traits::Nib;
use nibbler::nibble::Nibble;
let val: u8 = 10;
let mut nib: Nibble = Nibble::from(val);
assert_eq!(10, nib.into());

use nibbler::nibbles::Nibbles;

let val: u16 = 0xaa; // 10101010
let nibs: Nibbles = val.into();
assert_eq!(2, nibs.len());
assert!(nibs.fits_u8());
assert_eq!(170, nibs.into());
assert_eq!(10, nibs.get(0).into());
assert_eq!(10, nibs.get(1).into());

let val: u16 = 0xba; // 10111010
let nibs: Nibbles = val.into();
assert_eq!(2, nibs.len());
assert!(nibs.fits_u8());
assert_eq!(186, nibs.into());
assert_eq!(11, nibs.get(0).into());
assert_eq!(10, nibs.get(1).into());

let val: u16 = 0xf00;
let nibs: Nibbles = val.into();
assert_eq!(3, nibs.len());
assert!(!nibs.fits_u8());
assert!(nibs.fits_u16());
assert_eq!(3840, nibs.into());
assert_eq!(15, nibs.get(0).into());
assert_eq!(0, nibs.get(1).into());
assert_eq!(0, nibs.get(2).into());
```

Nibbles don't care about signing. Nibbles are just 4 bits. Signed integers are treated as unsigned integers in all conversions. You can cast back `.into()` a signed integer, as available.

For more information, see the [documentation](https://docs.rs/nibbler)... or on [Crates.io](https://crates.io/crates/nibbler) ... or the [repo](https://gitlab.com/RustyChalk/nibbler).

## Future

On the purported **"todo"** list is to add right and left shift, addition, subtraction, multiplication.

## License

GPLv3
