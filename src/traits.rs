
pub trait Nib {
    /// Returns an unsigned 8-bit integer value.
    fn ival(&self) -> u8;

    /// Returns a hex-dec string.
    fn hval(&self) -> String;

    /// Returns a binary string, MSB (left) -to- LSB (right)
    fn sval(&self) -> String;

    /// Gets the least significant bit, 2^0
    fn b0(&self) -> bool;

    /// Gets the 2^1 value.
    fn b1(&self) -> bool;

    /// Gets the 2^2 value.
    fn b2(&self) -> bool;

    /// Gets the most sigificant bit, 2^3
    fn b3(&self) -> bool;
    
    /// Gets the least significant bit, 2^0, as an integer (1 or 0).
    fn i0(&self) -> u8;
    
    /// Gets the 2^1 value as an integer (1 or 0).
    fn i1(&self) -> u8;
    
    /// Gets the 2^2 value as an integer (1 or 0).
    fn i2(&self) -> u8;
    
    /// Gets the most significant bit, 2^3, as an integer (1 or 0).
    fn i3(&self) -> u8;
    
    /// Gets the bit string, Little Endian, MSBit on left, LSBit on right.
    fn as_bit_str_le(&self) -> String;
    
    /// Gets the bit string, Big Endian, LSBit on left, MSBit on right.
    fn as_bit_str_be(&self) -> String;
}

