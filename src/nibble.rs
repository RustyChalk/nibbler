
use std::fmt;
use std::ops;

use crate::traits::Nib;

/// A struct containing 4 boolean values.
/// Each value represents a bit.
/// 
/// * bit0 = 2^0 (least significant bit)
/// * bit1 = 2^1
/// * bit2 = 2^2
/// * bit3 = 2^3 (most significant bit)
///
/// All mathematical operations, such as Add, Sub, Mul, Div...
/// go through the eye of conversion to u8 or usize.
///
/// If a u8 or usize would overflow or throw an error, this will throw an error.
/// If a signed integer, such as i8 would throw an error, this will throw an error.
///
/// * Implemented: Add, Sub, Mul, Div, Rem, Shr, Shl, And, Or, Xor, including Assign.
/// * Implemented: Index, IndexMut
/// * Implemented, Custom: **Not**; inverts internal bit values.
/// * Specific: Neg is specifically `unimplemented`
/// * Not Implemented: Range, Bounds, Deref, Fn, Experimental/Nightly(Coerce,Dispatch,Generator,Try)
///
#[derive(PartialEq,Copy,Clone,Debug)]
pub struct Nibble {
    bit0: bool, 
    bit1: bool, 
    bit2: bool, 
    bit3: bool
}

impl Nibble {
    /// Returns a new instance of the nibble struct.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::traits::Nib;
    /// use nibbler::nibble::Nibble;
    /// let mut nib = Nibble::new(false, false, false, false);
    /// assert_eq!(0, nib.into());
    /// nib = Nibble::new(true, false, false, false);
    /// assert_eq!(1, nib.into());
    /// assert_eq!(nib.b0(), true);
    /// assert_eq!(nib.i0(), 1);
    /// nib = Nibble::new(false, true, false, false);
    /// assert_eq!(2, nib.into());
    /// assert_eq!(nib.b0(), false);
    /// assert_eq!(nib.b1(), true);
    /// assert_eq!(nib.i0(), 0);
    /// assert_eq!(nib.i1(), 1);
    /// nib = Nibble::new(false, false, true, false);
    /// assert_eq!(4, nib.into());
    /// assert_eq!(nib.b0(), false);
    /// assert_eq!(nib.b1(), false);
    /// assert_eq!(nib.b2(), true);
    /// assert_eq!(nib.b3(), false);
    /// assert_eq!(nib.i0(), 0);
    /// assert_eq!(nib.i1(), 0);
    /// assert_eq!(nib.i2(), 1);
    /// assert_eq!(nib.i3(), 0);
    /// nib = Nibble::new(false, false, false, true);
    /// assert_eq!(8, nib.into());
    /// assert_eq!(nib.b0(), false);
    /// assert_eq!(nib.b1(), false);
    /// assert_eq!(nib.b2(), false);
    /// assert_eq!(nib.b3(), true);
    /// assert_eq!(nib.i0(), 0);
    /// assert_eq!(nib.i1(), 0);
    /// assert_eq!(nib.i2(), 0);
    /// assert_eq!(nib.i3(), 1);
    /// nib = Nibble::new(true, true, true, true);
    /// assert_eq!(15, nib.into());
    /// ```
    ///
    /// Realistically, or pragmatically, using true/false statements to 
    /// initialize an instance is not doable.
    ///
    /// ```rust
    /// use nibbler::traits::Nib;
    /// use nibbler::nibble::Nibble;
    /// let val: u8 = 15;
    /// let mut nib = Nibble::from(val);
    /// assert_eq!(15, nib.into());
    /// assert_eq!(nib.b0(), true);
    /// assert_eq!(nib.b1(), true);
    /// assert_eq!(nib.b2(), true);
    /// assert_eq!(nib.b3(), true);
    /// assert_eq!(nib.i0(), 1);
    /// assert_eq!(nib.i1(), 1);
    /// assert_eq!(nib.i2(), 1);
    /// assert_eq!(nib.i3(), 1);
    /// let val: u8 = 16;
    /// //nib = Nibble::from(val) // this fails.
    /// // 16 is 2^4, which is 5-bits; a nibble is only 4-bits.
    /// // 15 is the largest 2^3 value possible (4-bits).
    /// ```
    ///
    /// Opposed to the core struct here, `Nibble`, you probably want 
    /// `Nibbles`, which will turn any {u, i}{8, 16, 32, 64} into a Vec<Nibble>, 
    /// and convert back and forth.
    pub fn new(bit0: bool, bit1: bool, bit2: bool, bit3: bool) -> Nibble {
        Nibble { bit0, bit1, bit2, bit3 }
    }

    /// Returns a binary string, MSB (left) -to- LSB (right)
    ///
    /// * If the return value is `0001` the integer value is `1`
    /// * If the return value is `1000` the integer value is `8`
    ///
    pub fn to_string(&self) -> String {
        self.sval().to_string()
    }
   
    /// Returns a hex-dec string in 0x0..0xf
    ///
    /// * 0u8 == 0x0 == `0000`
    /// * 1u8 == 0x1 == `0001`
    /// * ...
    /// * 10u8 == 0xA == `1010`
    /// * ...
    /// * 15u8 == 0xF == `1111`
    ///
    pub fn to_hex_string(&self) -> String {
        self.hval().to_string()
    }

    pub fn to_hex_char(&self) -> char {
        let tmp: Vec<char> = self.to_hex_string().chars().collect();
        tmp[2]
    }
}

impl Nib for Nibble {
    fn ival(&self) -> u8 {
        let mut out = 0u8;
        if self.bit3 { out += 1; }
        out <<= 1;
        if self.bit2 { out += 1; }
        out <<= 1;
        if self.bit1 { out += 1; }
        out <<= 1;
        if self.bit0 { out += 1; }
        out
    }
    
    fn hval(&self) -> String {
        match self.ival() {
            0 => String::from("0x0"),
            1 => String::from("0x1"),
            2 => String::from("0x2"),
            3 => String::from("0x3"),
            4 => String::from("0x4"),
            5 => String::from("0x5"),
            6 => String::from("0x6"),
            7 => String::from("0x7"),
            8 => String::from("0x8"),
            9 => String::from("0x9"),
            10 => String::from("0xA"),
            11 => String::from("0xB"),
            12 => String::from("0xC"),
            13 => String::from("0xD"),
            14 => String::from("0xE"),
            15 => String::from("0xF"),
            _ => panic!("Invalid value!")
        }
    }

    fn sval(&self) -> String {
        match self.ival() {
            0 => String::from("0000"),
            1 => String::from("0001"),
            2 => String::from("0010"),
            3 => String::from("0011"),
            4 => String::from("0100"),
            5 => String::from("0101"),
            6 => String::from("0110"),
            7 => String::from("0111"),
            8 => String::from("1000"),
            9 => String::from("1001"),
            10 => String::from("1010"),
            11 => String::from("1011"),
            12 => String::from("1100"),
            13 => String::from("1101"),
            14 => String::from("1110"),
            15 => String::from("1111"),
            _ => panic!("Invalid u8 from Nibble?"),
        }
    }

    fn b0(&self) -> bool { self.bit0.clone() }
    fn b1(&self) -> bool { self.bit1.clone() }
    fn b2(&self) -> bool { self.bit2.clone() }
    fn b3(&self) -> bool { self.bit3.clone() }

    fn i0(&self) -> u8 { self.bit0.clone() as u8 }
    fn i1(&self) -> u8 { self.bit1.clone() as u8 }
    fn i2(&self) -> u8 { self.bit2.clone() as u8 }
    fn i3(&self) -> u8 { self.bit3.clone() as u8 }

    /// Gets the bit string, Little Endian, MSBit on left, LSBit on right.
    fn as_bit_str_le(&self) -> String {
        format!("{}{}{}{}", self.i3(), self.i2(), self.i1(), self.i0())
    }
    
    /// Gets the bit string, Big Endian, LSBit on left, MSBit on right.
    fn as_bit_str_be(&self) -> String {
        format!("{}{}{}{}", self.i0(), self.i1(), self.i2(), self.i3())
    }
}

impl fmt::Display for Nibble {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}{}{}{}", self.i3(), self.i2(), self.i1(), self.i0())
    }
}

impl From<u64> for Nibble {
    fn from(val: u64) -> Nibble {
        match val {
            0 => Nibble::new(false, false, false, false),
            1 => Nibble::new(true, false, false, false),
            2 => Nibble::new(false, true, false, false),
            3 => Nibble::new(true, true, false, false),
            4 => Nibble::new(false, false, true, false),
            5 => Nibble::new(true, false, true, false),
            6 => Nibble::new(false, true, true, false),
            7 => Nibble::new(true, true, true, false),
            8 => Nibble::new(false, false, false, true),
            9 => Nibble::new(true, false, false, true),
            10 => Nibble::new(false, true, false, true),
            11 => Nibble::new(true, true, false, true),
            12 => Nibble::new(false, false, true, true),
            13 => Nibble::new(true, false, true, true),
            14 => Nibble::new(false, true, true, true),
            15 => Nibble::new(true, true, true, true),
            _ => panic!("Use a mask to shift a u8 into two nibble parts before converting to nibble. Must be <= 15."),
                //Err("Use a mask to shift a u8 into two nibble parts before converting to nibble. Must be <= 15."),
        }
    }
}

impl From<u32> for Nibble {
    fn from(val: u32) -> Nibble {
        Nibble::from(val as u64)
    }
}

impl From<u16> for Nibble {
    fn from(val: u16) -> Nibble {
        Nibble::from(val as u64)
    }
}

impl From<u8> for Nibble {
    fn from(val: u8) -> Nibble {
        Nibble::from(val as u64)
    }
}

impl From<i64> for Nibble {
    fn from(val: i64) -> Nibble {
        Nibble::from(val as u64)
    }
}

impl From<i32> for Nibble {
    fn from(val: i32) -> Nibble {
        Nibble::from(val as u64)
    }
}

impl From<i16> for Nibble {
    fn from(val: i16) -> Nibble {
        Nibble::from(val as u64)
    }
}

impl From<i8> for Nibble {
    fn from(val: i8) -> Nibble {
        Nibble::from(val as u64)
    }
}

impl From<Nibble> for u64 {
    fn from(n: Nibble) -> u64 {
        match (n.b0(), n.b1(), n.b2(), n.b3()) {
            (false, false, false, false) => 0u64,
            (true, false, false, false) => 1u64,
            (false, true, false, false) => 2u64,
            (true, true, false, false) => 3u64,
            (false, false, true, false) => 4u64,
            (true, false, true, false) => 5u64,
            (false, true, true, false) => 6u64,
            (true, true, true, false) => 7u64,
            (false, false, false, true) => 8u64,
            (true, false, false, true) => 9u64,
            (false, true, false, true) => 10u64,
            (true, true, false, true) => 11u64,
            (false, false, true, true) => 12u64,
            (true, false, true, true) => 13u64,
            (false, true, true, true) => 14u64,
            (true, true, true, true) => 15u64,
        }
    }
}

impl From<Nibble> for i64 {
    fn from(n: Nibble) -> i64 {
        let tmp: u64 = n.into();
        tmp as i64
    }
}

impl From<Nibble> for u32 {
    fn from(n: Nibble) -> u32 {
        let tmp: u64 = n.into();
        tmp as u32
    }
}

impl From<Nibble> for i32 {
    fn from(n: Nibble) -> i32 {
        let tmp: u64 = n.into();
        tmp as i32
    }
}

impl From<Nibble> for u16 {
    fn from(n: Nibble) -> u16 {
        let tmp: u64 = n.into();
        tmp as u16
    }
}

impl From<Nibble> for i16 {
    fn from(n: Nibble) -> i16 {
        let tmp: u64 = n.into();
        tmp as i16
    }
}

impl From<Nibble> for u8 {
    fn from(n: Nibble) -> u8 {
        let tmp: u64 = n.into();
        tmp as u8
    }
}

impl From<Nibble> for i8 {
    fn from(n: Nibble) -> i8 {
        let tmp: u64 = n.into();
        tmp as i8
    }
}

impl From<char> for Nibble {
    fn from(c: char) -> Nibble {
        match c {
            '0' => Nibble::from(0),
            '1' => Nibble::from(1),
            '2' => Nibble::from(2),
            '3' => Nibble::from(3),
            '4' => Nibble::from(4),
            '5' => Nibble::from(5),
            '6' => Nibble::from(6),
            '7' => Nibble::from(7),
            '8' => Nibble::from(8),
            '9' => Nibble::from(9),
            'a' => Nibble::from(10),
            'b' => Nibble::from(11),
            'c' => Nibble::from(12),
            'd' => Nibble::from(13),
            'e' => Nibble::from(14),
            'f' => Nibble::from(15),
            'A' => Nibble::from(10),
            'B' => Nibble::from(11),
            'C' => Nibble::from(12),
            'D' => Nibble::from(13),
            'E' => Nibble::from(14),
            'F' => Nibble::from(15),
            _ => panic!(format!("invalid hex-dec character: `{}`", c)),
        }
    }
}

impl From<Nibble> for char {
    fn from(n: Nibble) -> char {
        let tmp: &str = &(n.to_string());
        match tmp {
            "0000" => '0',
            "0001" => '1',
            "0010" => '2',
            "0011" => '3',
            "0100" => '4',
            "0101" => '5',
            "0110" => '6',
            "0111" => '7',
            "1000" => '8',
            "1001" => '9',
            "1010" => 'A',
            "1011" => 'B',
            "1100" => 'C',
            "1101" => 'D',
            "1110" => 'E',
            "1111" => 'F',
            _ => panic!("Nibble did not convert to a char?"),
        }
    }
}

impl From<usize> for Nibble {
    fn from(u: usize) -> Nibble {
        Nibble::from(u as u64) 
    }
}

impl From<Nibble> for usize {
    fn from(n: Nibble) -> usize {
        let tmp: u64 = n.into();
        tmp as usize
    }
}

impl ops::Add<Nibble> for Nibble {
    type Output = Self;

    fn add(self, other: Self) -> Nibble {
        let a: u8 = self.into();
        let b: u8 = other.into();
        (a+b).into()
    }
}

impl ops::AddAssign<Nibble> for Nibble {
    fn add_assign(&mut self, other: Self) {
        *self = *self+other
    }
}

impl ops::Add<usize> for Nibble {
    type Output = Self;

    fn add(self, other: usize) -> Nibble {
        let a: usize = self.into();
        (a+other).into()
    }
}

impl ops::AddAssign<usize> for Nibble {
    fn add_assign(&mut self, other: usize) {
        *self = *self+other
    }
}

impl ops::Sub<Nibble> for Nibble {
    type Output = Self;

    fn sub(self, other: Self) -> Nibble {
        let a: u8 = self.into();
        let b: u8 = other.into();
        (a-b).into()
    }
}

impl ops::SubAssign<Nibble> for Nibble {
    fn sub_assign(&mut self, other: Self) {
        *self = *self-other
    }
}

impl ops::Sub<usize> for Nibble {
    type Output = Self;

    fn sub(self, other: usize) -> Nibble {
        let a: usize = self.into();
        (a-other).into()
    }
}

impl ops::SubAssign<usize> for Nibble {
    fn sub_assign(&mut self, other: usize) {
        *self = *self-other
    }
}

impl ops::Mul<Nibble> for Nibble {
    type Output = Self;

    fn mul(self, other: Self) -> Nibble {
        let a: u8 = self.into();
        let b: u8 = other.into();
        (a*b).into()
    }
}

impl ops::MulAssign<Nibble> for Nibble {
    fn mul_assign(&mut self, other: Self) {
        *self = *self*other
    }
}

impl ops::Mul<usize> for Nibble {
    type Output = Self;

    fn mul(self, other: usize) -> Nibble {
        let a: usize = self.into();
        (a*other).into()
    }
}

impl ops::MulAssign<usize> for Nibble {
    fn mul_assign(&mut self, other: usize) {
        *self = *self*other
    }
}

impl ops::Div<Nibble> for Nibble {
    type Output = Self;

    fn div(self, other: Self) -> Nibble {
        let a: u8 = self.into();
        let b: u8 = other.into();
        (a/b).into()
    }
}

impl ops::DivAssign<Nibble> for Nibble {
    fn div_assign(&mut self, other: Self) {
        *self = *self/other
    }
}

impl ops::Div<usize> for Nibble {
    type Output = Self;

    fn div(self, other: usize) -> Nibble {
        let a: usize = self.into();
        (a/other).into()
    }
}

impl ops::DivAssign<usize> for Nibble {
    fn div_assign(&mut self, other: usize) {
        *self = *self/other
    }
}

impl ops::Rem<usize> for Nibble {
    type Output = Self;

    fn rem(self, modulus: usize) -> Self::Output {
        let a: usize = self.into();
        (a%modulus).into()
    }
}

impl ops::RemAssign<usize> for Nibble {
    fn rem_assign(&mut self, modulus: usize) {
        *self = *self%modulus;
    }
}

impl ops::Shl<usize> for Nibble {
    type Output = Self;

    fn shl(self, rhs: usize) -> Nibble {
        if rhs >= 4 {
            panic!("Shift: n-1 bits; a nibble has 4-bits; you can only shift a max of 3 without overflow.");
        }
        let mut tmp: u8 = self.into();
        tmp <<= rhs;
        tmp.into()
    }
}

impl ops::ShlAssign<usize> for Nibble {
    fn shl_assign(&mut self, rhs: usize) {
        *self = *self << rhs;
    }
}

impl ops::Shr<usize> for Nibble {
    type Output = Self;

    fn shr(self, rhs: usize) -> Nibble {
        if rhs >= 4 {
            panic!("Shift: n-1 bits; a nibble has 4-bits; you can only shift a max of 3 without overflow.");
        }
        let mut tmp: u8 = self.into();
        tmp >>= rhs;
        tmp.into()
    }
}

impl ops::ShrAssign<usize> for Nibble {
    fn shr_assign(&mut self, rhs: usize) {
        *self = *self >> rhs;
    }
}

impl ops::BitAnd<Nibble> for Nibble {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Nibble {
        let a: u8 = self.into();
        let b: u8 = rhs.into();
        (a&b).into()
    }
}

impl ops::BitAndAssign<Nibble> for Nibble {
    fn bitand_assign(&mut self, rhs: Self) {
        *self = *self&rhs
    }
}

impl ops::BitOr<Nibble> for Nibble {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Nibble {
        let a: u8 = self.into();
        let b: u8 = rhs.into();
        (a|b).into()
    }
}

impl ops::BitOrAssign<Nibble> for Nibble {
    fn bitor_assign(&mut self, rhs: Self) {
        *self = *self|rhs
    }
}

impl ops::BitXor<Nibble> for Nibble {
    type Output = Self;

    fn bitxor(self, rhs: Self) -> Nibble {
        let a: u8 = self.into();
        let b: u8 = rhs.into();
        (a^b).into()
    }
}

impl ops::BitXorAssign<Nibble> for Nibble {
    fn bitxor_assign(&mut self, rhs: Self) {
        *self = *self^rhs
    }
}

/// Inverts the internal bit values.
/// Returns a new instance.
impl ops::Not for Nibble {
    type Output = Self;

    /// Inverts the internal bit values.
    /// Returns a new instance.
    /// 
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibble::Nibble;
    /// let tst: Nibble = Nibble::from(10u8);
    /// let not_tst = !tst;
    /// assert_eq!(5, not_tst.into());
    /// let tst: Nibble = Nibble::from(4u8);
    /// let not_tst = !tst;
    /// assert_eq!(11, not_tst.into());
    /// ```
    ///
    fn not(self) -> Nibble {
        Nibble::new(!self.b0(), !self.b1(), !self.b2(), !self.b3())
    }
}

impl ops::Neg for Nibble {
    type Output = Self;

    fn neg(self) -> Nibble {
        let m0 = "Negation is not implemented for the `Nibble` object.";
        let m1 = "`Nibble` only handles absolute (unsigned) 4-bit integers.";
        unimplemented!("{} {}", m0, m1);
    }
}

impl ops::Index<usize> for Nibble {
    type Output = bool;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.bit0,
            1 => &self.bit1,
            2 => &self.bit2,
            3 => &self.bit3,
            _ => panic!("Index error: Nibble is addressed as 0..3, having only 4-bits."),
        }
    }
}

impl ops::IndexMut<usize> for Nibble {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            0 => &mut self.bit0,
            1 => &mut self.bit1,
            2 => &mut self.bit2,
            3 => &mut self.bit3,
            _ => panic!("Index error: Nibble is addressed as 0..3, having only 4-bits."),
        }
    }
}

#[cfg(test)]
mod tests {

    use crate::traits::Nib;
    use crate::nibble::Nibble;

    #[test]
    fn new_nibble() {
        let nib = Nibble::new(false, false, false, false);
        let val: u64 = nib.into();
        assert_eq!(val, 0);
        assert_eq!(nib.b0(), false);
        assert_eq!(nib.b1(), false);
        assert_eq!(nib.b2(), false);
        assert_eq!(nib.b3(), false);
        assert_eq!(nib.i0(), 0);
        assert_eq!(nib.i1(), 0);
        assert_eq!(nib.i2(), 0);
        assert_eq!(nib.i3(), 0);
        assert_eq!(nib.hval(), "0x0");
        assert_eq!(nib.ival(), 0);
        assert_eq!(val, nib.ival().into());
    }

    #[test]
    fn new_nibble_1() {
        let nib = Nibble::new(true, false, false, false);
        let val: u8 = nib.into();
        assert_eq!(val, 1);
        assert_eq!(nib.b0(), true);
        assert_eq!(nib.b1(), false);
        assert_eq!(nib.b2(), false);
        assert_eq!(nib.b3(), false);
        assert_eq!(nib.i0(), 1);
        assert_eq!(nib.i1(), 0);
        assert_eq!(nib.i2(), 0);
        assert_eq!(nib.i3(), 0);
        assert_eq!(nib.hval(), "0x1");
        assert_eq!(nib.ival(), 1);
        assert_eq!(val, nib.ival().into());
    }
    
    #[test]
    fn new_nibble_2() {
        let nib = Nibble::new(false, true, false, false);
        let val: u8 = nib.into();
        assert_eq!(val, 2);
        assert_eq!(nib.b0(), false);
        assert_eq!(nib.b1(), true);
        assert_eq!(nib.b2(), false);
        assert_eq!(nib.b3(), false);
        assert_eq!(nib.i0(), 0);
        assert_eq!(nib.i1(), 1);
        assert_eq!(nib.i2(), 0);
        assert_eq!(nib.i3(), 0);
        assert_eq!(nib.hval(), "0x2");
        assert_eq!(nib.ival(), 2);
        assert_eq!(val, nib.ival().into());
    }
    
    #[test]
    fn new_nibble_4() {
        let nib = Nibble::new(false, false, true, false);
        let val: u8 = nib.into();
        assert_eq!(val, 4);
        assert_eq!(nib.b0(), false);
        assert_eq!(nib.b1(), false);
        assert_eq!(nib.b2(), true);
        assert_eq!(nib.b3(), false);
        assert_eq!(nib.i0(), 0);
        assert_eq!(nib.i1(), 0);
        assert_eq!(nib.i2(), 1);
        assert_eq!(nib.i3(), 0);
        assert_eq!(nib.hval(), "0x4");
        assert_eq!(nib.ival(), 4);
        assert_eq!(val, nib.ival().into());
    }
    
    #[test]
    fn new_nibble_5() {
        let nib = Nibble::new(true, false, true, false);
        let val: u8 = nib.into();
        assert_eq!(val, 5);
        assert_eq!(nib.b0(), true);
        assert_eq!(nib.b1(), false);
        assert_eq!(nib.b2(), true);
        assert_eq!(nib.b3(), false);
        assert_eq!(nib.i0(), 1);
        assert_eq!(nib.i1(), 0);
        assert_eq!(nib.i2(), 1);
        assert_eq!(nib.i3(), 0);
        assert_eq!(nib.hval(), "0x5");
        assert_eq!(nib.ival(), 5);
        assert_eq!(val, nib.ival().into());
    }
    
    #[test]
    fn new_nibble_8() {
        let nib = Nibble::new(false, false, false, true);
        let val: u8 = nib.into();
        assert_eq!(val, 8);
        assert_eq!(nib.b0(), false);
        assert_eq!(nib.b1(), false);
        assert_eq!(nib.b2(), false);
        assert_eq!(nib.b3(), true);
        assert_eq!(nib.i0(), 0);
        assert_eq!(nib.i1(), 0);
        assert_eq!(nib.i2(), 0);
        assert_eq!(nib.i3(), 1);
        assert_eq!(nib.hval(), "0x8");
        assert_eq!(nib.ival(), 8);
        assert_eq!(val, nib.ival().into());
    }
    
    #[test]
    fn new_nibble_a10() {
        let nib = Nibble::new(false, true, false, true);
        let val: u8 = nib.into();
        assert_eq!(val, 10);
        assert_eq!(nib.b0(), false);
        assert_eq!(nib.b1(), true);
        assert_eq!(nib.b2(), false);
        assert_eq!(nib.b3(), true);
        assert_eq!(nib.i0(), 0);
        assert_eq!(nib.i1(), 1);
        assert_eq!(nib.i2(), 0);
        assert_eq!(nib.i3(), 1);
        assert_eq!(nib.hval(), "0xA");
        assert_eq!(nib.ival(), 10);
        assert_eq!(val, nib.ival().into());
    }
    
    #[test]
    fn new_nibble_f15() {
        let nib = Nibble::new(true, true, true, true);
        let val: u8 = nib.into();
        assert_eq!(val, 15);
        assert_eq!(nib.b0(), true);
        assert_eq!(nib.b1(), true);
        assert_eq!(nib.b2(), true);
        assert_eq!(nib.b3(), true);
        assert_eq!(nib.i0(), 1);
        assert_eq!(nib.i1(), 1);
        assert_eq!(nib.i2(), 1);
        assert_eq!(nib.i3(), 1);
        assert_eq!(nib.hval(), "0xF");
        assert_eq!(nib.ival(), 15);
        assert_eq!(val, nib.ival().into());
    }
    
    #[test]
    fn new_nibble_2_neq_1() {
        let nib = Nibble::new(false, true, false, false);
        let val: u8 = nib.into();
        assert!(val != 1);
    }
    
    #[test]
    fn nibble_le_str() {
        let nib = Nibble::new(true, false, false, false);
        let val = nib.as_bit_str_le();
        assert_eq!(val, "0001");
    }
    
    #[test]
    fn nibble_be_str() {
        let nib = Nibble::new(true, false, false, false);
        let val = nib.as_bit_str_be();
        assert_eq!(val, "1000");
    }

    #[test]
    fn nibble_from_u8() {
        let val = 3u8;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }

    #[test]
    #[should_panic]
    fn nibble_from_invalid_u8_should_panic() {
        let val = 16u8;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }

    #[test]
    fn nibble_from_u16() {
        let val = 4u16;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }

    #[test]
    #[should_panic]
    fn nibble_from_invalid_u16_should_panic() {
        let val = 17u16;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }

    #[test]
    fn nibble_from_u32() {
        let val = 5u32;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }
    
    #[test]
    #[should_panic]
    fn nibble_from_invalid_u32_should_panic() {
        let val = 32u8;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }

    #[test]
    fn nibble_from_u64() {
        let val = 6u64;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }
    
    #[test]
    #[should_panic]
    fn nibble_from_invalid_u64_should_panic() {
        let val = 64u8;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }

    #[test]
    fn nibble_from_i8() {
        let val = 3i8;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }

    #[test]
    #[should_panic]
    fn nibble_from_invalid_i8_should_panic() {
        let val = -3i8;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }

    #[test]
    fn nibble_from_i16() {
        let val = 4i16;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }

    #[test]
    #[should_panic]
    fn nibble_from_invalid_i16_should_panic() {
        let val = -4i16;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }

    #[test]
    fn nibble_from_i32() {
        let val = 5i32;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }
    
    #[test]
    #[should_panic]
    fn nibble_from_invalid_i32_should_panic() {
        let val = -5i32;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }

    #[test]
    fn nibble_from_i64() {
        let val = 6i64;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }

    #[test]
    #[should_panic]
    fn nibble_from_invalid_i64_should_panic() {
        let val = -6i64;
        let tst = Nibble::from(val);
        assert_eq!(val, tst.into());
    }

    #[test]
    fn nibble_to_u8() {
        let tst = Nibble::new(false, true, false, true);
        let val: u8 = tst.into();
        let v2 = u8::from(tst);
        assert_eq!(tst, val.into());
        assert_eq!(val, v2);
        assert_eq!(v2, 10);
    }

    #[test]
    fn nibble_to_u16() {
        let tst = Nibble::new(false, true, true, false);
        let val: u16 = tst.into();
        let v2 = u16::from(tst);
        assert_eq!(tst, val.into());
        assert_eq!(val, v2);
        assert_eq!(v2, 6);
    }

    #[test]
    fn nibble_to_u32() {
        let tst = Nibble::new(true, true, true, false);
        let val: u32 = tst.into();
        let v2 = u32::from(tst);
        assert_eq!(tst, val.into());
        assert_eq!(val, v2);
        assert_eq!(v2, 7);
    }

    #[test]
    fn nibble_to_u64() {
        let tst = Nibble::new(true, true, true, true);
        let val: u64 = tst.into();
        let v2 = u64::from(tst);
        assert_eq!(tst, val.into());
        assert_eq!(val, v2);
        assert_eq!(v2, 15);
    }

    #[test]
    fn nibble_to_i8() {
        let tst = Nibble::new(true, false, true, false);
        let val: i8 = tst.into();
        let v2 = i8::from(tst);
        assert_eq!(tst, val.into());
        assert_eq!(val, v2);
        assert_eq!(v2, 5);
    }

    #[test]
    fn nibble_to_i16() {
        let tst = Nibble::new(true, false, true, true);
        let val: i16 = tst.into();
        let v2 = i16::from(tst);
        assert_eq!(tst, val.into());
        assert_eq!(val, v2);
        assert_eq!(v2, 13);
    }

    #[test]
    fn nibble_to_i32() {
        let tst = Nibble::new(false, false, true, true);
        let val: i32 = tst.into();
        let v2 = i32::from(tst);
        assert_eq!(tst, val.into());
        assert_eq!(val, v2);
        assert_eq!(v2, 12);
    }

    #[test]
    fn nibble_to_i64() {
        let tst = Nibble::new(false, false, false, true);
        let val: i64 = tst.into();
        let v2 = i64::from(tst);
        assert_eq!(tst, val.into());
        assert_eq!(val, v2);
        assert_eq!(v2, 8);
    }

    /* ========== ADDITION ========== */

    #[test]
    fn nibble_ops_add() {
        let a: Nibble = 5u8.into();
        let b: Nibble = 3u8.into();
        let c = a+b;
        assert_eq!(8, c.into());
    }
    
    #[test]
    #[should_panic]
    fn nibble_ops_add_overflow_panic() {
        let a: Nibble = 11u8.into();
        let b: Nibble = 5u8.into();
        let _c = a+b;
    }
    
    #[test]
    fn nibble_ops_add_assign() {
        let mut a: Nibble = 5u8.into();
        let b: Nibble = 3u8.into();
        a += b;
        assert_eq!(8, a.into());
    }

    #[test]
    #[should_panic]
    fn nibble_ops_add_assign_overflow_panic() {
        let mut a: Nibble = 11u8.into();
        let b: Nibble = 5u8.into();
        a += b;
    }

    /* ========== SUBTRACTION ========== */

    #[test]
    fn nibble_ops_sub() {
        let a: Nibble = 5u8.into();
        let b: Nibble = 3u8.into();
        let c = a-b;
        assert_eq!(2, c.into());
    }
    
    #[test]
    #[should_panic]
    fn nibble_ops_sub_overflow_panic() {
        let a: Nibble = 5u8.into();
        let b: Nibble = 3u8.into();
        let _c = b-a;
    }
    
    #[test]
    fn nibble_ops_sub_assign() {
        let mut a: Nibble = 5u8.into();
        let b: Nibble = 3u8.into();
        a -= b;
        assert_eq!(2, a.into());
    }

    #[test]
    #[should_panic]
    fn nibble_ops_sub_assign_overflow_panic() {
        let mut a: Nibble = 5u8.into();
        let b: Nibble = 11u8.into();
        a -= b;
    }

    /* ========== MULTIPLICATION ========== */

    #[test]
    fn nibble_ops_mul() {
        let a: Nibble = 5u8.into();
        let b: Nibble = 3u8.into();
        let c = a*b;
        assert_eq!(15, c.into());
    }
    
    #[test]
    #[should_panic]
    fn nibble_ops_mul_overflow_panic() {
        let a: Nibble = 11u8.into();
        let b: Nibble = 5u8.into();
        let _c = a*b;
    }
    
    #[test]
    fn nibble_ops_mul_assign() {
        let mut a: Nibble = 5u8.into();
        let b: Nibble = 3u8.into();
        a *= b;
        assert_eq!(15, a.into());
    }

    #[test]
    #[should_panic]
    fn nibble_ops_mul_assign_overflow_panic() {
        let mut a: Nibble = 11u8.into();
        let b: Nibble = 5u8.into();
        a *= b;
    }
    
    /* ========== DIVISION ========== */

    #[test]
    fn nibble_ops_div() {
        let a: Nibble = 5u8.into();
        let b: Nibble = 3u8.into();
        let c = a/b;
        assert_eq!(1, c.into());
        let a: Nibble = 6u8.into();
        let b: Nibble = 3u8.into();
        let c = a/b;
        assert_eq!(2, c.into());
        let a: Nibble = 0u8.into();
        let b: Nibble = 3u8.into();
        let c = a/b;
        assert_eq!(0, c.into());
    }
    
    #[test]
    #[should_panic]
    fn nibble_ops_div0_panic() {
        let a: Nibble = 11u8.into();
        let b: Nibble = 0u8.into();
        let _c = a/b;
    }
    
    #[test]
    fn nibble_ops_div_assign() {
        let mut a: Nibble = 5u8.into();
        let b: Nibble = 3u8.into();
        a /= b;
        assert_eq!(1, a.into());
        let mut a: Nibble = 6u8.into();
        let b: Nibble = 3u8.into();
        a /= b;
        assert_eq!(2, a.into());
        let mut a: Nibble = 0u8.into();
        let b: Nibble = 3u8.into();
        a /= b;
        assert_eq!(0, a.into());
    }

    #[test]
    #[should_panic]
    fn nibble_ops_div0_assign_panic() {
        let mut a: Nibble = 11u8.into();
        let b: Nibble = 0u8.into();
        a /= b;
    }
    
    /* ========== Shift Left (*2) ========== */

    #[test]
    fn nibble_ops_shl() {
        let tst: Nibble = 1.into();
        let t2: Nibble = tst << 1;
        assert_eq!(2, t2.into());
        let t3: Nibble = t2 << 1;
        assert_eq!(4, t3.into());
        let t4: Nibble = t3 << 1;
        assert_eq!(8, t4.into());
        let t5: Nibble = 5u8.into();
        let t10: Nibble = t5 << 1;
        assert_eq!(10, t10.into());
        let t8: Nibble = tst << 3;
        assert_eq!(8, t8.into());
    }

    #[test]
    #[should_panic]
    fn nibble_ops_shl_overflow_panic() {
        let tst: Nibble = 1.into();
        let _ovr: Nibble = tst << 4;
    }

    #[test]
    fn nibble_ops_shr() {
        let tst: Nibble = 8.into();
        let t2: Nibble = tst >> 1;
        assert_eq!(4, t2.into());
        let t3: Nibble = t2 >> 1;
        let t4: Nibble = tst >> 2;
        assert_eq!(t3, t4);
        let t5: Nibble = t4 >> 1;
        assert_eq!(1, t5.into());
        let t6: Nibble = t5 >> 1;
        assert_eq!(0, t6.into());
        let t7: Nibble = t6 >> 1;
        assert_eq!(0, t7.into());
    }

    #[test]
    #[should_panic]
    fn nibble_ops_shr_overflow_panic() {
        let tst: Nibble = 15.into();
        let _ovr: Nibble = tst >> 4;
    }

    #[test]
    #[should_panic]
    fn nibble_ops_neg() {
        let mut _tst: Nibble = Nibble::from(1u8);
        _tst = -_tst;
    }
}

