
use std::fmt;

use crate::traits::Nib;
use crate::nibble::Nibble;

/// Holds a vector (array) of Nibble values.
/// Second value is boolean; whether or not the object is editable 
/// (in addition to mutability concerns).
///
/// If instantiated with an integer: Cannot be edited.
/// If instantiated with one of the `::new_` methods, 
/// and declared mutable, then the objects vector is editable.
#[derive(PartialEq,Clone,Debug)]
pub struct Nibbles(Vec<Nibble>, bool);

impl Nibbles {
    /// Creates a new instance of nibbles.
    /// Can be modified.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let t1 = Nibbles::new();
    /// assert_eq!(t1.len(), 1);
    /// assert_eq!(0, t1.get(0).into());
    /// ```
    ///
    pub fn new() -> Nibbles {
        Nibbles(vec![0u8.into(); 1], true)
    }

    /// Creates a new instance from a vector of nibbles.
    /// Can be modified.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let t1 = Nibbles::from(5u8);
    /// let t2 = Nibbles::new_from_nib_vec(t1.raw_vec());
    /// assert!(t1 != t2);
    /// assert_eq!(t1.raw_vec(), t2.raw_vec());  // vector is self.0 -> Vec<Nibble>
    /// assert!(t1.editable() != t2.editable()); // editable is self.1 -> bool
    /// // t1.push(0.into()); // fails, panics, not editable.
    /// assert!(t2.editable());
    /// // t2.push(0.into()); // fails, t2 is not mutable.
    /// let mut t3 = Nibbles::new_from_nib_vec(t2.raw_vec());
    /// assert_eq!(5, t1.clone().into());
    /// assert_eq!(5, t2.clone().into());
    /// assert_eq!(5, t3.clone().into());
    /// assert_eq!(t2, t3); // vector and editable are the same, unlike t1; however t3 is mutable.
    /// t3.push(0.into());
    /// assert!(t2 != t3);
    /// assert_eq!(80, t3.clone().into());
    /// ```
    ///
    pub fn new_from_nib_vec(nv: Vec<Nibble>) -> Nibbles {
        Nibbles(nv, true)
    }

    /// Creates a new instance from an existing Nibbles instance.
    /// Can be modified.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let t1 = Nibbles::from(5u8);
    /// let t2 = Nibbles::new_from_self(&t1);
    /// assert!(t1 != t2);
    /// assert_eq!(t1.raw_vec(), t2.raw_vec());  // vector is self.0 -> Vec<Nibble>
    /// assert!(t1.editable() != t2.editable()); // editable is self.1 -> bool
    /// // t1.push(0.into()); // fails, panics, not editable.
    /// assert!(t2.editable());
    /// // t2.push(0.into()); // fails, t2 is not mutable.
    /// let mut t3 = Nibbles::new_from_self(&t2);
    /// assert_eq!(5, t1.clone().into());
    /// assert_eq!(5, t2.clone().into());
    /// assert_eq!(5, t3.clone().into());
    /// assert_eq!(t2, t3); // vector and editable are the same, unlike t1; however t3 is mutable.
    /// t3.push(0.into());
    /// assert!(t2 != t3);
    /// assert_eq!(80, t3.clone().into());
    /// ```
    ///
    pub fn new_from_self(&self) -> Nibbles {
        Nibbles(self.0.clone(), true)
    }

    /// Creates a new instance of Nibbles with a specified capacity.
    /// Can be modified.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let t1 = Nibbles::with_capacity(5);
    /// assert_eq!(t1.len(), 0);
    /// assert_eq!(t1.capacity(), 5);
    /// // but you can't do anything with this, t1 is not mutable. It is a dead object.
    /// let mut t2 = Nibbles::with_capacity(5);
    /// assert_eq!(t2.len(), 0);
    /// assert_eq!(t2.capacity(), 5);
    /// t2.push(5.into());
    /// assert_eq!(t2.len(), 1);
    /// assert_eq!(t2.capacity(), 5);
    /// let five = t2.pop();
    /// assert_eq!(t2.len(), 0);
    /// assert_eq!(t2.capacity(), 5);
    /// assert_eq!(5, five.into());
    /// t2.push(0.into());
    /// t2.push(1.into());
    /// t2.push(2.into());
    /// t2.push(3.into());
    /// t2.push(4.into());
    /// assert_eq!(t2.len(), 5);
    /// assert_eq!(t2.capacity(), 5);
    /// t2.push(5.into());
    /// assert!(t2.capacity() >= 6);
    /// // capacity increased to accomodate a 6th item; additional allocation determinted by rust
    /// ```
    ///
    pub fn with_capacity(size: usize) -> Nibbles {
        //Nibbles(vec![0u8.into(); size], true)
        Nibbles(Vec::with_capacity(size), true)
    }

    /// Creates a new instance of Nibbles from a hex-dec string.
    /// Can be modified.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let shreck: Nibbles = Nibbles::new_from_hex_str("0xff");
    /// assert_eq!(2, shreck.len());
    /// assert_eq!(15, shreck.get(0).into());
    /// assert_eq!(15, shreck.get(1).into());
    /// assert_eq!(255, shreck.clone().into());
    /// let shreck: Nibbles = Nibbles::new_from_hex_str("0xABCDEF0123456789fedcab");
    /// assert_eq!(22, shreck.len());
    /// assert_eq!(10, shreck.get(0).into());
    /// assert_eq!(11, shreck.get(1).into());
    /// assert_eq!(12, shreck.get(2).into());
    /// assert_eq!(13, shreck.get(3).into());
    /// assert_eq!(14, shreck.get(4).into());
    /// assert_eq!(15, shreck.get(5).into());
    /// assert_eq!(0, shreck.get(6).into());
    /// assert_eq!(1, shreck.get(7).into());
    /// assert_eq!(2, shreck.get(8).into());
    /// assert_eq!(3, shreck.get(9).into());
    /// assert_eq!(4, shreck.get(10).into());
    /// assert_eq!(5, shreck.get(11).into());
    /// assert_eq!(6, shreck.get(12).into());
    /// assert_eq!(7, shreck.get(13).into());
    /// assert_eq!(8, shreck.get(14).into());
    /// assert_eq!(9, shreck.get(15).into());
    /// assert_eq!(15, shreck.get(16).into());
    /// assert_eq!(14, shreck.get(17).into());
    /// assert_eq!(13, shreck.get(18).into());
    /// assert_eq!(12, shreck.get(19).into());
    /// assert_eq!(10, shreck.get(20).into());
    /// assert_eq!(11, shreck.get(21).into());
    /// // let shreck: Nibbles = Nibbles::new_from_hex_str("0xabcdexxx"); // panic
    /// // notice the xxx at the end of the string: That's not a hex-dec character.
    /// ```
    ///
    pub fn new_from_hex_str(s: &str) -> Nibbles {
        assert!(s.starts_with("0x"), format!("Hex-dec string {} did not start with `0x`", s));
        let chars: Vec<char> = s.chars().collect();
        let mut out: Nibbles = Nibbles::with_capacity(chars.len()-2);
        for c in 2..chars.len() {
            out.push(chars[c].into());
        }
        out
    }

    /// Convert a vector of Nibble to a hex-dec string.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let tst = "0xabcdef0123456789fedcab".to_string();
    /// let t1: Nibbles = Nibbles::new_from_hex_str(&tst);
    /// let out = t1.to_hex_str();
    /// assert_eq!(tst, out.to_lowercase());
    /// let tst = "0x0abcdef0123456789fedcab".to_string();
    /// let t2: Nibbles = Nibbles::new_from_hex_str(&tst);
    /// let out = t2.to_hex_str();
    /// assert!(tst != out.to_lowercase());
    /// // the latter example is not equal becase a 0x0 prefixes the former.
    /// // the leading 0 is removed, being irrelevant toward absolute value.
    /// ```
    pub fn to_hex_str(&self) -> String {
        let mut hex = String::from("0x");
        let mut out = String::from("");
        for nib in self.0.iter() {
            out.push_str(&nib.to_hex_char().to_string());
        }
        hex.push_str(out.trim_start_matches('0'));
        hex
    }

    /// Converts a raw binary string (0, 1) into a vector of Nibble.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let t1: Nibbles = Nibbles::new_from_bin_str("101");
    /// assert_eq!(5, t1.clone().into());
    /// assert!(t1.fits_u8());
    /// let t2: Nibbles = Nibbles::new_from_bin_str("1011010");
    /// assert_eq!(90, t2.clone().into());
    /// assert!(t2.fits_u8());
    /// let t3: Nibbles = Nibbles::new_from_bin_str("10110100101");
    /// assert_eq!(1445, t3.clone().into());
    /// assert!(!t3.fits_u8());
    /// assert!(t3.fits_u16());
    /// let mut t4: Nibbles = Nibbles::new_from_bin_str("1010101111001101111011110000000100100011010001010110011110001001111111101101110010101011");
    /// // 0xabcdef0123456789fedcab
    /// assert!(!t4.fits_u64());
    /// assert!(t4.is_bignum());
    /// assert_eq!(t4.len(), 22);
    /// for _i in 0..6 {
    ///     t4.remove(0);
    /// }
    /// // 0x0123456789fedcab
    /// assert!(t4.fits_u64());
    /// assert_eq!(t4.len(), 16);
    /// if 0 == t4.get(0).into() {
    ///     t4.shrink();
    /// }
    /// // 0x123456789fedcab
    /// assert_eq!(15, t4.len());
    /// let t5: Nibbles = Nibbles::new_from_hex_str("0x123456789fedcab");
    /// assert_eq!(t4, t5);
    /// ```
    ///
    pub fn new_from_bin_str(s: &str) -> Nibbles {
        //assert!(s.starts_with("1"), "binary strings should remove unnecessary MSB padding; must start with 1");
        let m = s.len()%4;
        let d = (s.len()-&m)/4;
        let mut cap = d;
        if m > 0 {
            cap += 1;
        }
        let mut out: Nibbles = Nibbles::with_capacity(cap);
        if m > 0 {
            out.push(u64::from_str_radix(&s[0..m], 2).unwrap().into());
        }
        for i in 0..d {
            let v = i*4+m;
            out.push(u64::from_str_radix(&s[v..v+4], 2).unwrap().into());
        }
        out.shrink();
        out
    }

    /// Convert a vector of Nibble to a binary string.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let val = "1010101111001101111011110000000100100011010001010110011110001001111111101101110010101011";
    /// let tst: Nibbles = Nibbles::new_from_bin_str(val);
    /// let out = tst.to_bin_str();
    /// assert_eq!(val, out);
    /// let val = "010110100101";
    /// let tst: Nibbles = Nibbles::new_from_bin_str(val);
    /// let out = tst.to_bin_str();
    /// assert!(val != out);
    /// ```
    ///
    pub fn to_bin_str(&self) -> String {
        let mut out = String::from("");
        for nib in self.0.iter() {
            out.push_str(&nib.to_string());
        }
        out.trim_start_matches('0').to_string()
    }

    /// Makes the object not editable (even if mutable).
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let mut t1 = Nibbles::with_capacity(5);
    /// t1.push(0.into());
    /// t1.set_static();
    /// //t1.push(1.into()); // this will fail; the editable flag has been set to false.
    /// //t1.pop(); // this will fail; the editable flag has been set to false.
    /// ```
    ///
    pub fn set_static(&mut self) {
        if self.1 {
            self.1 = false;
        } 
    }

    /// Get count of nibbles.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let t1 = Nibbles::new();
    /// assert_eq!(t1.len(), 1);
    /// assert_eq!(0, t1.get(0).into());
    /// ```
    ///
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Get the capacity of the internal vector.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let t1 = Nibbles::with_capacity(5);
    /// assert_eq!(t1.len(), 0);
    /// assert_eq!(t1.capacity(), 5);
    /// // but you can't do anything with this, t1 is not mutable. It is a dead object.
    /// let mut t2 = Nibbles::with_capacity(5);
    /// assert_eq!(t2.len(), 0);
    /// assert_eq!(t2.capacity(), 5);
    /// t2.push(5.into());
    /// assert_eq!(t2.len(), 1);
    /// assert_eq!(t2.capacity(), 5);
    /// let five = t2.pop();
    /// assert_eq!(t2.len(), 0);
    /// assert_eq!(t2.capacity(), 5);
    /// assert_eq!(5, five.into());
    /// t2.push(0.into());
    /// t2.push(1.into());
    /// t2.push(2.into());
    /// t2.push(3.into());
    /// t2.push(4.into());
    /// assert_eq!(t2.len(), 5);
    /// assert_eq!(t2.capacity(), 5);
    /// t2.push(5.into());
    /// assert!(t2.capacity() >= 6);
    /// // capacity increased to accomodate a 6th item; additional allocation determinted by rust
    /// ```
    ///
    pub fn capacity(&self) -> usize {
        self.0.capacity()
    }

    /// Checks if the nibbles will fit into a u8 (vector <= 2 values)
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let mut tst = Nibbles::new();
    /// assert!(tst.fits_u8());
    /// assert_eq!(tst.len(), 1);
    /// let pop = tst.pop();
    /// assert_eq!(tst.len(), 0);
    /// assert!(tst.fits_u8());
    /// tst.push(pop);
    /// tst.push(1.into());
    /// assert_eq!(tst.len(), 2);
    /// assert!(tst.fits_u8());
    /// tst.push(2.into());
    /// assert_eq!(tst.len(), 3);
    /// assert!(!tst.fits_u8());
    /// assert!(tst.fits_u16());
    /// ```
    ///
    pub fn fits_u8(&self) -> bool {
        self.0.len() <= 2
    }

    /// Checks if the nibbles will fit into a u16 (vector <= 4 values)
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let t1: Nibbles = 256u16.into();
    /// let mut t2 = Nibbles::new_from_self(&t1);
    /// assert!(!t2.fits_u8());
    /// assert!(t2.fits_u16());
    /// assert_eq!(t2.len(), 4);
    /// t2.shrink();
    /// assert_eq!(t2.len(), 3);
    /// let pop = t2.pop();
    /// assert_eq!(t2.len(), 2);
    /// assert!(t2.fits_u8());
    /// t2.push(pop);
    /// t2.push(4.into());
    /// assert_eq!(t2.len(), 4);
    /// assert!(!t2.fits_u8());
    /// assert!(t2.fits_u16());
    /// t2.push(5.into());
    /// assert_eq!(t2.len(), 5);
    /// assert!(!t2.fits_u8());
    /// assert!(!t2.fits_u16());
    /// assert!(t2.fits_u32());
    /// ```
    ///
    pub fn fits_u16(&self) -> bool {
        self.0.len() <= 4
    }

    /// Checks if the nibbles will fit into a u32 (vector <= 8 values)
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let t1: Nibbles = 65536u32.into();
    /// let mut t2 = Nibbles::new_from_self(&t1);
    /// assert!(!t2.fits_u8());
    /// assert!(!t2.fits_u16());
    /// assert!(t2.fits_u32());
    /// assert_eq!(t2.len(), 8);
    /// t2.shrink();
    /// assert_eq!(t2.len(), 5);
    /// let pop = t2.pop();
    /// assert_eq!(t2.len(), 4);
    /// assert!(!t2.fits_u8());
    /// assert!(t2.fits_u16());
    /// assert!(t2.fits_u32());
    /// t2.push(pop);
    /// assert!(!t2.fits_u16());
    /// assert!(t2.fits_u32());
    /// t2.push(6.into());
    /// assert_eq!(t2.len(), 6);
    /// assert!(t2.fits_u32());
    /// t2.push(7.into());
    /// t2.push(8.into());
    /// t2.push(9.into());
    /// assert_eq!(t2.len(), 9);
    /// assert!(!t2.fits_u8());
    /// assert!(!t2.fits_u16());
    /// assert!(!t2.fits_u32());
    /// assert!(t2.fits_u64());
    /// ```
    ///
    pub fn fits_u32(&self) -> bool {
        self.0.len() <= 8
    }
    
    /// Checks if the nibbles will fit into a u64 (vector <= 16 values)
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let t1: Nibbles = 4294967296u64.into();
    /// let mut t2 = Nibbles::new_from_self(&t1);
    /// assert!(!t2.fits_u8());
    /// assert!(!t2.fits_u16());
    /// assert!(!t2.fits_u32());
    /// assert!(t2.fits_u64());
    /// assert_eq!(t2.len(), 16);
    /// t2.shrink();
    /// assert_eq!(t2.len(), 9);
    /// let pop = t2.pop();
    /// assert_eq!(t2.len(), 8);
    /// assert!(!t2.fits_u8());
    /// assert!(!t2.fits_u16());
    /// assert!(t2.fits_u32());
    /// assert!(t2.fits_u64());
    /// t2.push(pop);
    /// assert!(!t2.fits_u32());
    /// assert!(t2.fits_u64());
    /// t2.push(10.into());
    /// assert_eq!(t2.len(), 10);
    /// assert!(t2.fits_u64());
    /// t2.push(11.into()); // 10+1 == 11
    /// t2.push(12.into()); // ...2
    /// t2.push(13.into()); // ...3
    /// t2.push(14.into()); // ...4
    /// t2.push(15.into()); // ...5
    /// // t2.push(16.into()); // this would fail; 0 <= Nibble <= 15
    /// t2.push(0.into());  // ...6
    /// t2.push(1.into());  // ...7 == 17
    /// assert_eq!(t2.len(), 17);
    /// assert!(!t2.fits_u8());
    /// assert!(!t2.fits_u16());
    /// assert!(!t2.fits_u32());
    /// assert!(!t2.fits_u64());
    /// assert!(t2.is_bignum());
    /// ```
    ///
    pub fn fits_u64(&self) -> bool {
        self.0.len() <= 16
    }

    /// Checks to see if this is a bignum (nibbles count > 16)
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let val: u64 = 18364758544493064720;
    /// let t1: Nibbles = val.into();
    /// let mut t2: Nibbles = Nibbles::new_from_self(&t1);
    /// t2.push(1.into());
    /// assert!(t2.capacity() >= 17);
    /// assert!(!t2.fits_u64());
    /// assert!(t2.is_bignum());
    /// let _tmp = t2.pop();
    /// assert!(t2.capacity() >= 17);
    /// assert!(t2.fits_u64());
    /// assert!(!t2.is_bignum());
    /// ```
    ///
    pub fn is_bignum(&self) -> bool {
        self.0.len() > 16
    }

    /// Returns a clone of the internal nibble vector.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let val: u64 = 18364758544493064720;
    /// let t1 = Nibbles::from(val);
    /// let t2 = Nibbles::new_from_nib_vec(t1.raw_vec());
    /// assert_eq!(t1.len(), 16);
    /// assert_eq!(t2.len(), 16);
    /// assert_eq!(t1.raw_vec(), t2.raw_vec());
    /// assert!(t1.editable() != t2.editable() && t2.editable());
    /// ```
    pub fn raw_vec(&self) -> Vec<Nibble> {
        self.0.clone()
    }

    /// Returns whether or not the object is editable.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let val: u64 = 18364758544493064720;
    /// let mut t1 = Nibbles::from(val);
    /// let t2 = Nibbles::new_from_nib_vec(t1.raw_vec());
    /// assert_eq!(t1.len(), 16);
    /// assert_eq!(t2.len(), 16);
    /// assert_eq!(t1.raw_vec(), t2.raw_vec());
    /// assert!(t1.editable() != t2.editable() && t2.editable());
    /// let mut t3 = Nibbles::new_from_self(&t1);
    /// assert_eq!(t2.raw_vec(), t3.raw_vec());
    /// assert!(t1.editable() != t3.editable() && t2.editable() == t3.editable() && t3.editable());
    /// // t1.set(0, 0.into()); // This will panic, because t1 is not editable.
    /// // t2.set(0, 0.into()); // This will not work, because t2 is not mutable.
    /// t3.set(0, 0.into());
    /// assert_eq!(1070935975390360080u64, t3.into());
    /// // bits 2^63, 2^62, 2^61 and 2^60 have been zero'ed out (0).
    /// ```
    ///
    /// In the example above, `t1` was instantiated from an integer. 
    /// It is not editable. The new instance, `t2`, manually instantiated 
    /// from the `t1` vector, is editable. A simpler example below...
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let tst = Nibbles::new();
    /// assert_eq!(tst.len(), 1);
    /// assert_eq!(0, tst.get(0).into());
    /// assert!(tst.editable());
    /// ```
    ///
    pub fn editable(&self) -> bool {
        self.1.clone()
    }

    /// Get the value at a specified index.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let val: u64 = 18364758544493064720;
    /// let t1 = Nibbles::from(val);
    /// let get = t1.get(0);
    /// assert_eq!(15, get.into());
    /// ```
    ///
    pub fn get(&self, idx: usize) -> Nibble {
        self.0[idx].clone()
    }

    /// Set the value at a specified index, if editable.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let val: u64 = 18364758544493064720;
    /// let t1 = Nibbles::from(val); // not mutable, not editable
    /// let mut t2 = Nibbles::new_from_self(&t1);
    /// t2.set(0, 0.into());
    /// // MSB bits 2^63, 2^62, 2^61 and 2^60 have been zero'ed out (0).
    /// assert_eq!(1070935975390360080u64, t2.into());
    /// ```
    ///
    pub fn set(&mut self, idx: usize, val: Nibble) {
        if self.1 {
            self.0[idx] = val;
        } else {
            panic!("Not editable!")
        }
    }

    /// Push a Nibble onto the end of the vector.
    /// Expands internal vector capacity as necessary.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let mut tst = Nibbles::new();
    /// assert_eq!(tst.len(), 1);
    /// tst.set(0, 3.into());
    /// assert!(tst.fits_u8());
    /// assert_eq!(3, tst.clone().into());
    /// tst.push(4.into());
    /// assert!(tst.fits_u8());
    /// assert_eq!(52, tst.clone().into());
    /// tst.push(5.into());
    /// assert!(!tst.fits_u8());
    /// assert!(tst.fits_u16());
    /// assert_eq!(837, tst.clone().into());
    /// ```
    ///
    pub fn push(&mut self, val: Nibble) {
        if self.1 {
            self.0.push(val);
        } else {
            panic!("Not editable!")
        }
    }

    /// Pop an item off the end of the vector, if populated.
    /// If there's nothing to pop, standard error is issued.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let mut tst = Nibbles::new();
    /// assert_eq!(tst.len(), 1);
    /// let pop = tst.pop();
    /// assert_eq!(tst.len(), 0);
    /// assert_eq!(0, pop.into());
    /// ```
    ///
    pub fn pop(&mut self) -> Nibble {
        if self.1 {
            self.0.pop().unwrap()
        } else {
            panic!("Not editable!")
        }
    }

    /// Remove an item.
    ///
    /// Example Usage:
    ///
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let val: u64 = 18364758544493064720;
    /// let t1 = Nibbles::from(val); // not mutable, not editable
    /// let mut t2 = Nibbles::new_from_self(&t1);
    /// t2.remove(0);
    /// // MSB bits 2^63, 2^62, 2^61 and 2^60 have been removed (wholesale).
    /// assert_eq!(1070935975390360080u64, t2.into());
    /// ```
    ///
    pub fn remove(&mut self, idx: usize) {
        if self.1 {
            if idx < self.len() {
                self.0.remove(idx);
            }
        } else {
            panic!("Not editable!")
        }
    }

    /// Shrink all MSNib==0000
    ///
    /// Example Usage:
    /// ```rust
    /// use nibbler::nibbles::Nibbles;
    /// let mut nib = Nibbles::new(); // 1
    /// assert_eq!(nib.len(), 1);
    /// nib.push(0.into()); // 2
    /// nib.push(0.into()); // 3
    /// nib.push(0.into()); // 4
    /// for i in (0..16_u8).rev() { // this line looks really, really bad...
    /// // but for the fact the above line excludes 16, 
    /// // and 15 is inclusive of a u8 (unsigned; byte).
    ///     nib.push(i.into());
    /// }
    /// assert!(nib.is_bignum());
    /// assert!(!nib.fits_u64());
    /// assert_eq!(nib.len(), 16+4); //20*4-bits-per-nibble == 80-bits.
    /// nib.shrink(); // get rid of all those pesky MSB==0000
    /// assert_eq!(18364758544493064720u64, nib.clone().into());
    /// assert_eq!(nib.len(), 16);
    /// assert!(nib.fits_u64());
    /// ```
    ///
    /// The `shrink` function will not remove right most, or LSB nibbles, 
    /// as this would be inaccurate and give smaller numeric values. 
    /// Truncating LSB should be an explicit activity. Removing extraneous 
    /// leading zeros on the left side of the array still gives the same 
    /// numeric value.
    ///
    pub fn shrink(&mut self) {
        if self.1 {
            let nib0 = Nibble::new(false, false, false, false);
            while self.get(0) == nib0 {
                self.remove(0);
            }
        } else {
            panic!("Not editable!")
        }
    }
}

impl From<u64> for Nibbles {
    fn from(val: u64) -> Nibbles {
        let mut out: Vec<Nibble> = vec![Nibble::new(false, false, false, false); 16];
        out[0] = ((val&0xf000000000000000) >> 60).into();
        out[1] = ((val&0xf00000000000000) >> 56).into();
        out[2] = ((val&0xf0000000000000) >> 52).into();
        out[3] = ((val&0xf000000000000) >> 48).into();
        out[4] = ((val&0xf00000000000) >> 44).into();
        out[5] = ((val&0xf0000000000) >> 40).into();
        out[6] = ((val&0xf000000000) >> 36).into();
        out[7] = ((val&0xf00000000) >> 32).into();
        out[8] = ((val&0xf0000000) >> 28).into();
        out[9] = ((val&0xf000000) >> 24).into();
        out[10] = ((val&0xf00000) >> 20).into();
        out[11] = ((val&0xf0000) >> 16).into();
        out[12] = ((val&0xf000) >> 12).into();
        out[13] = ((val&0xf00) >> 8).into();
        out[14] = ((val&0xf0) >> 4).into();
        out[15] = ((val&0xf)).into();
        Nibbles(out, false)
    }
}

impl From<i64> for Nibbles {
    fn from(val: i64) -> Nibbles {
        Nibbles::from(val as u64)
    }
}

impl From<u32> for Nibbles {
    fn from(val: u32) -> Nibbles {
        let mut out: Vec<Nibble> = vec![Nibble::new(false, false, false, false); 8];
        out[0] = ((val&0xf0000000) >> 28).into();
        out[1] = ((val&0xf000000) >> 24).into();
        out[2] = ((val&0xf00000) >> 20).into();
        out[3] = ((val&0xf0000) >> 16).into();
        out[4] = ((val&0xf000) >> 12).into();
        out[5] = ((val&0xf00) >> 8).into();
        out[6] = ((val&0xf0) >> 4).into();
        out[7] = ((val&0xf)).into();
        Nibbles(out, false)
    }
}

impl From<i32> for Nibbles {
    fn from(val: i32) -> Nibbles {
        Nibbles::from(val as u32)
    }
}

impl From<u16> for Nibbles {
    fn from(val: u16) -> Nibbles {
        let mut out: Vec<Nibble> = vec![Nibble::new(false, false, false, false); 4];
        out[0] = ((val&0xf000) >> 12).into();
        out[1] = ((val&0xf00) >> 8).into();
        out[2] = ((val&0xf0) >> 4).into();
        out[3] = ((val&0xf)).into();
        Nibbles(out, false)
    }
}

impl From<i16> for Nibbles {
    fn from(val: i16) -> Nibbles {
        Nibbles::from(val as u16)
    }
}

impl From<u8> for Nibbles {
    fn from(val: u8) -> Nibbles {
        let mut out: Vec<Nibble> = vec![Nibble::new(false, false, false, false); 2];
        out[0] = ((val&0xf0) >> 4).into();
        out[1] = ((val&0x0f)).into();
        Nibbles(out, false)
    }
}

impl From<i8> for Nibbles {
    fn from(val: i8) -> Nibbles {
        Nibbles::from(val as u8)
    }
}

impl Into<u8> for Nibbles {
    fn into(self) -> u8 {
        if self.fits_u8() {
            let out: u64 = self.into();
            out as u8
        } else {
            panic!("Nibbles length >= 2; did you first check if the value would fit into this type?")
        }
    }
}

impl Into<i8> for Nibbles {
    fn into(self) -> i8 {
        let out: u8 = self.into();
        out as i8
    }
}

impl Into<u16> for Nibbles {
    fn into(self) -> u16 {
        if self.fits_u16() {
            let out: u64 = self.into();
            out as u16
        } else {
            panic!("Nibbles length >= 4; did you first check if the value would fit into this type?")
        }
    }
}

impl Into<i16> for Nibbles {
    fn into(self) -> i16 {
        let out: u16 = self.into();
        out as i16
    }
}

impl Into<u32> for Nibbles {
    fn into(self) -> u32 {
        if self.fits_u32() {
            let out: u64 = self.into();
            out as u32
        } else {
            panic!("Nibbles length >= 8; did you first check if the value would fit into this type?")
        }
    }
}

impl Into<i32> for Nibbles {
    fn into(self) -> i32 {
        let out: u32 = self.into();
        out as i32
    }
}

impl Into<u64> for Nibbles {
    fn into(self) -> u64 {
        if self.fits_u64() {
            if self.len() > 0 {
                let mut out: u64 = 0u64;
                for i in 0..self.0.len() {
                    out <<= 4;
                    out |= self.0[i].ival() as u64;
                }
                out
            } else {
                0 as u64
            }
        } else {
            panic!("Nibbles length >= 16; did you first check if the value would fit into this type?")
        }
    }
}

impl Into<i64> for Nibbles {
    fn into(self) -> i64 {
        let out: u64 = self.into();
        out as i64
    }
}

impl fmt::Display for Nibbles {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = "";
        for i in 0..self.0.len() {
            format!("{} {}", s, self.0[i].to_string());
        }
        write!(f, "{}", s)
    }
}

#[cfg(test)]
mod tests {

    //use crate::traits::Nib;
    //use crate::nibble::Nibble;
    use crate::nibbles::Nibbles;

    #[test]
    fn nibbles_new() {
        let tst = Nibbles::new();
        assert_eq!(tst.len(), 1);
        assert_eq!(0, tst.get(0).into());
        assert!(tst.editable());
    }

    #[test]
    fn nibbles_new_from_nib_vec_raw() {
        let val: u64 = 18364758544493064720;
        let t1 = Nibbles::from(val);
        let t2 = Nibbles::new_from_nib_vec(t1.raw_vec());
        assert_eq!(t1.len(), 16);
        assert_eq!(t2.len(), 16);
        assert_eq!(t1.raw_vec(), t2.raw_vec());
        assert!(t1.editable() != t2.editable() && t2.editable());
    }

    #[test]
    fn nibbles_new_from_self() {
        let val: u64 = 18364758544493064720;
        let t1 = Nibbles::from(val);
        let t2 = Nibbles::new_from_self(&t1);
        assert_eq!(t1.len(), 16);
        assert_eq!(t2.len(), 16);
        assert_eq!(t1.raw_vec(), t2.raw_vec());
        assert!(t1.editable() != t2.editable() && t2.editable());
    }

    #[test]
    fn nibbles_new_from_u8() {
        let val: u8 = 241;
        let tst = Nibbles::from(val);
        assert_eq!(tst.len(), 2);
        assert_eq!(15, tst.get(0).into());
        assert_eq!(1, tst.get(1).into());
        assert!(!tst.editable());
        assert_eq!(val, tst.into());
    }
    
    #[test]
    fn nibbles_new_from_u16() {
        let val: u16 = 63552;
        let tst = Nibbles::from(val);
        assert_eq!(tst.len(), 4);
        assert_eq!(15, tst.get(0).into());
        assert_eq!(8, tst.get(1).into());
        assert_eq!(4, tst.get(2).into());
        assert_eq!(0, tst.get(3).into());
        assert!(!tst.editable());
        assert_eq!(val, tst.into());
    }
    
    #[test]
    fn nibbles_new_from_u32() {
        let val: u32 = 4275816753;
        let tst = Nibbles::from(val);
        assert_eq!(tst.len(), 8);
        assert_eq!(15, tst.get(0).into());
        assert_eq!(14, tst.get(1).into());
        assert_eq!(13, tst.get(2).into());
        assert_eq!(11, tst.get(3).into());
        assert_eq!(12, tst.get(4).into());
        assert_eq!(9, tst.get(5).into());
        assert_eq!(3, tst.get(6).into());
        assert_eq!(1, tst.get(7).into());
        assert!(!tst.editable());
        assert_eq!(val, tst.into());
    }
    
    #[test]
    fn nibbles_new_from_u64() {
        let val: u64 = 18364758544493064720;
        let tst = Nibbles::from(val);
        assert_eq!(tst.len(), 16);
        assert_eq!(15, tst.get(0).into());
        assert_eq!(14, tst.get(1).into());
        assert_eq!(13, tst.get(2).into());
        assert_eq!(12, tst.get(3).into());
        assert_eq!(11, tst.get(4).into());
        assert_eq!(10, tst.get(5).into());
        assert_eq!(9, tst.get(6).into());
        assert_eq!(8, tst.get(7).into());
        assert_eq!(7, tst.get(8).into());
        assert_eq!(6, tst.get(9).into());
        assert_eq!(5, tst.get(10).into());
        assert_eq!(4, tst.get(11).into());
        assert_eq!(3, tst.get(12).into());
        assert_eq!(2, tst.get(13).into());
        assert_eq!(1, tst.get(14).into());
        assert_eq!(0, tst.get(15).into());
        assert!(!tst.editable());
        assert_eq!(val, tst.into());
    }

    #[test]
    fn nibbles_new_with_capacity_push_pop_get_set_remove() {
        let mut tst = Nibbles::with_capacity(5);
        assert_eq!(tst.capacity(), 5);
        assert_eq!(tst.len(), 0);
        tst.push(5.into());
        assert_eq!(tst.len(), 1);
        tst.push(4.into());
        assert_eq!(tst.len(), 2);
        tst.push(3.into());
        assert_eq!(tst.len(), 3);
        //0101|0100|0011
        //1024+256|+|64|+|3
        assert_eq!(5, tst.get(0).into());
        assert_eq!(4, tst.get(1).into());
        assert_eq!(3, tst.get(2).into());
        assert_eq!(1347, tst.clone().into());
        tst.set(2, 4.into());
        assert_eq!(4, tst.get(2).into());
        assert_eq!(1348, tst.clone().into());
        let mut nib = tst.pop();
        assert_eq!(4, nib.into());
        assert_eq!(tst.len(), 2);
        assert_eq!(5, tst.get(0).into());
        assert_eq!(4, tst.get(1).into());
        assert_eq!(84, tst.clone().into());
        tst.remove(1);
        assert_eq!(tst.len(), 1);
        assert_eq!(5, tst.get(0).into());
        assert_eq!(5, tst.clone().into());
        nib = tst.pop();
        assert_eq!(5, nib.into());
        assert_eq!(tst.len(), 0);
        assert_eq!(tst.capacity(), 5);
    }

    #[test]
    #[should_panic]
    fn nibbles_u64_into_u32_should_panic() {
        let val: u64 = 18364758544493064720;
        let tst = Nibbles::from(val);
        let _bad_conversion: u32 = tst.into();
    }
    
    #[test]
    #[should_panic]
    fn nibbles_u32_into_u16_should_panic() {
        let val: u32 = 4275816753;
        let tst = Nibbles::from(val);
        let _bad_conversion: u16 = tst.into();
    }
    
    #[test]
    #[should_panic]
    fn nibbles_u16_into_u8_should_panic() {
        let val: u16 = 63552;
        let tst = Nibbles::from(val);
        let _bad_conversion: u8 = tst.into();
    }
    
    #[test]
    fn nibbles_from_u32_into_u64() {
        let val: u32 = 4275816753;
        let tst = Nibbles::from(val);
        let upg: u64 = tst.into();
        assert_eq!(upg, val.into());
    }
    
    #[test]
    fn nibbles_from_u16_into_u32() {
        let val: u16 = 63552;
        let tst = Nibbles::from(val);
        let upg: u32 = tst.into();
        assert_eq!(upg, val.into());
    }
    
    #[test]
    fn nibbles_from_u8_into_u16() {
        let val: u8 = 241;
        let tst = Nibbles::from(val);
        let upg: u16 = tst.into();
        assert_eq!(upg, val.into());
    }
    
    #[test]
    fn nibbles_is_bignum() {
        let val: u64 = 18364758544493064720;
        let t1: Nibbles = val.into();
        let mut t2: Nibbles = Nibbles::new_from_self(&t1);
        t2.push(1.into());
        assert!(t2.capacity() >= 17);
        assert!(!t2.fits_u64());
        assert!(t2.is_bignum());
        let _tmp = t2.pop();
        assert!(t2.capacity() >= 17);
        assert!(t2.fits_u64());
        assert!(!t2.is_bignum());
    }

    #[test]
    fn nibbles_shrink() {
        let mut nib = Nibbles::new(); // 1
        assert_eq!(nib.len(), 1);
        nib.push(0.into()); // 2
        nib.push(0.into()); // 3
        nib.push(0.into()); // 4
        for i in (0..16_u8).rev() {
            nib.push(i.into());
        }
        assert!(nib.is_bignum());
        assert!(!nib.fits_u64());
        assert_eq!(nib.len(), 16+4);
        nib.shrink();
        assert_eq!(18364758544493064720u64, nib.clone().into());
    }

    #[test]
    fn nibbles_fits_u16() {
        let t1: Nibbles = 256u16.into();
        let mut t2 = Nibbles::new_from_self(&t1);
        assert!(!t2.fits_u8());
        assert!(t2.fits_u16());
        assert_eq!(t2.len(), 4);
        t2.shrink();
        assert_eq!(t2.len(), 3);
        let pop = t2.pop();
        assert_eq!(t2.len(), 2);
        assert!(t2.fits_u8());
        t2.push(pop);
        t2.push(4.into());
        assert_eq!(t2.len(), 4);
        assert!(!t2.fits_u8());
        assert!(t2.fits_u16());
        t2.push(5.into());
        assert_eq!(t2.len(), 5);
        assert!(!t2.fits_u8());
        assert!(!t2.fits_u16());
        assert!(t2.fits_u32());
    }

    #[test]
    fn nibbles_new_from_bin_str() {
        let t1: Nibbles = Nibbles::new_from_bin_str("101");
        assert_eq!(5, t1.clone().into());
        assert!(t1.fits_u8());
        let t2: Nibbles = Nibbles::new_from_bin_str("1011010");
        assert_eq!(90, t2.clone().into());
        assert!(t2.fits_u8());
        let t3: Nibbles = Nibbles::new_from_bin_str("10110100101");
        assert_eq!(1445, t3.clone().into());
        assert!(!t3.fits_u8());
        assert!(t3.fits_u16());
        let mut t4: Nibbles = Nibbles::new_from_bin_str("1010101111001101111011110000000100100011010001010110011110001001111111101101110010101011");
        // 0xabcdef0123456789fedcab
        assert!(!t4.fits_u64());
        assert!(t4.is_bignum());
        assert_eq!(t4.len(), 22);
        for _i in 0..6 {
            //let _tmp = t4.pop();
            t4.remove(0);
        }
        // 0x0123456789fedcab
        assert!(t4.fits_u64());
        assert_eq!(t4.len(), 16);
        if 0 == t4.get(0).into() {
            t4.shrink();
        }
        // 0x123456789fedcab
        assert_eq!(15, t4.len());
        let t5: Nibbles = Nibbles::new_from_hex_str("0x123456789fedcab");
        assert_eq!(t4, t5);
    }

    #[test]
    fn nibbles_to_bin_str() {
        let val = "1010101111001101111011110000000100100011010001010110011110001001111111101101110010101011";
        let tst: Nibbles = Nibbles::new_from_bin_str(val);
        let out = tst.to_bin_str();
        assert_eq!(val, out);
        let val = "010110100101";
        let tst: Nibbles = Nibbles::new_from_bin_str(val);
        let out = tst.to_bin_str();
        assert!(val != out);
    }

    #[test]
    fn nibbles_to_hex_str() {
        let tst = "0xabcdef0123456789fedcab".to_string();
        let t1: Nibbles = Nibbles::new_from_hex_str(&tst);
        let out = t1.to_hex_str();
        assert_eq!(tst, out.to_lowercase());
        let tst = "0x0abcdef0123456789fedcab";
        let t2: Nibbles = Nibbles::new_from_hex_str(tst);
        let out = t2.to_hex_str();
        assert!(tst != out);
    }
}
